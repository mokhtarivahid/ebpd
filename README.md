# Repository content  
This repository includes the materials for the experience-based planning and learning system.  
It includes the following files and folders for the 'robotic arm' application domain:  
  
1) the planning operators domain ('domain.ebpd')  
2) the abstract operators domain ('domain-abs.ebpd')  
3) a set of experiences ('experience.ebpd')  
4) a set of activity schemata, i.e., task models ('schema.ebpd')  
5) a set of problems represented in both ebpd and pddl (\problems)  
6) the obtained results from SBP, Mp and FD planners (\results)  
  
  
# Experience-Based Planning Domains (EBPDs)  
  
# Definition 1.  
An EBPD is a tuple,  
  
D = (L, Σ, S, A, O, E, M),  
  
where L is a first-order logic language that has finitely many predicate and constant symbols,  
Σ is a set of ground atoms of L that are always true (i.e., static world information), S is  
a set of states in which every state s ∈ S is a set of ground atoms of L which may become false  
(i.e., transient world information), A is a set of abstract operators, O is a set of planning  
operators, E is a set of experiences, and M is a set of methods in the form of activity schemata.  
  
# Definition 2.  
An abstract operator a ∈ A is a triple,  
  
a = (h, P, E),  
  
where h is the abstract operator's head, P is the precondition, and E is the effect of a. A head  
takes a form n(x1 , ..., xk≥0), in which n is the name, and x1 , ..., xk are the arguments, e.g.,  
pick(?object, ?table).  
  
# Definition 3.  
A planning operator o ∈ O is a tuple,  
  
o = (h, a, S, P, E),  
  
where h is the operator’s head, a is an abstract operator head which specifies the superclass or  
parent of o, S is the static world information, and P and E are respectively the preconditions and  
effects of o. A ground instance of an operator is called an action.  
  
  
# Definition 4.  
An experience e ∈ E is a triple of ground structures,  
  
e = (t, K, π),  
  
where t is the head of a task, taught by a human user to a robot, e.g., clear(table1), K is a set of  
key propositions, and π is a plan solution to achieve t. K is a subset of world description of an  
experience. Every key proposition in K is in the form τ (P ), which τ is a timestamp and P is a  
predicate. The timestamps specify the temporal extent of the predicates in an experience. Three types  
of timestamps are used to represent key propositions, init (true at the initial state, e.g., (init(on  
cup table1)), static (always true during an experience, e.g., (static(arm canreach arm1 table1))), and  
goal (true at the goal state, e.g., (goal(on cup tray1))).  
  
# Definition 5.  
An activity schema m ∈ M is a triple,  
  
m = (h, Ω),  
  
where h is a head of a target task (e.g., clear(?table)), and Ω is an abstract plan to achieve the h,  
i.e., a sequence or loops of abstract operators enriched with features.  
  
# Definition 6.  
An enriched abstract operator ω is a pair,  
  
ω = (a, F),  
  
where a ∈ A is an abstract operator head, and F is a set of features, i.e., a set of key propositions  
(see Definition 4), that describes the arguments of a.  
  
  
# Definition 7.  
A task planning problem is a tuple of ground structures,  
  
P = (t, σ, s0, g),  
  
where t is the head of a target task to be planned (e.g., clear(table1)), σ ⊆ Σ is the static world  
information (i.e., state invariant information), s0 ∈ S is the initial state (i.e., transient world  
information), and g is the goal, i.e., a set of propositions to be satisfied in a goal state sg ∈ S.  
